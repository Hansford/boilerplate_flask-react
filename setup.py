# !/usr/bin/env python

from distutils.core import setup

setup(
    name='Boilerplate',
    version='1.0.0',
    description='Hansford Web Dev Boilerplate Flask App - with Create React App',
    author='Jordan Hansford',
    author_email='jhansford@hansfordwebdev.com',
    license='MIT',
    url='https://gitlab.com/',
    keywords = [ 'fullstack', 'template', 'python3', 'react','npm', 'create-react-app'],
    classifiers = [
    'Topic :: Software Development',
    'Natural Language :: English'
    ],
    requires=['flask']
)
