#!/usr/bin/env python
"""Create a initial admin user"""
from getpass import getpass
from backend import db, app
from backend.models import User, UserRoles
import sys

def main():
    with app.app_context():
        db.metadata.create_all(db.engine)
        if User.query.all():
            create = input('A user already exists! Create another? (y/n):')
            if create == 'n':
                return
        username = input("Enter username:")
        email = input("Enter email address:")
        password = getpass()
        assert password == getpass('Password (again):')
        user = User(username=username, email=email)
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        roles = UserRoles(user_id=user.id, role_id=1)
        db.session.add(roles)
        db.session.commit()
        print("Admin user Created")
    

if __name__ == '__main__':
    sys.exit(main())
