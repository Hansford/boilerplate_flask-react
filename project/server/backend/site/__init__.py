from flask import Blueprint


bp = Blueprint('site', __name__,)

from backend.site import routes, events

