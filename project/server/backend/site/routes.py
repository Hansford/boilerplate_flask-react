from flask import render_template, flash, redirect, url_for, request
from backend import db, app, limiter
from backend.site import bp
from datetime import datetime
from flask_login import current_user, login_required

@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


# Landing page
@bp.route('/')
@login_required
@limiter.limit("1000/hour")
def index():
    return render_template("index.html")


