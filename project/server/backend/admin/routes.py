from flask import render_template, url_for, request, redirect
from backend.admin import bp
from backend import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required


#Leads Dashboard - Loads in the leads
@bp.route('/', methods=['GET', 'POST'])
@login_required
def admin():
    return render_template('admin/admin.html', title='Admin Page')


