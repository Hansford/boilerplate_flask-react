
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user, login_manager
from flask_migrate import Migrate
from flask_mail import Mail
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_cors import CORS
from functools import wraps
from flask_socketio import SocketIO
import os

#CONFIG EVENTLET
import eventlet
eventlet.monkey_patch()

'''
UTILS
'''

basedir = os.path.abspath(os.path.dirname(__file__))
 

'''
APP
DEFAULT: app = Flask(__name__) 
'''
app = Flask(__name__, static_folder='../../frontend/build/static', template_folder='../../frontend/templates')

'''
SOCKET-IO
Default to Redit with eventlet
'''
socketio = SocketIO() 
socketio.init_app(app, message_queue='redis://', async_mode = 'eventlet')

'''
APP CONFIGURATION
'''
app.config.from_object('config')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
app.config['DEBUG'] = True
app.secret_key = '45j3b45khjb34ajfbw38rgfuq30ugfqw3igfqno3ugfsfsvbvavFE34h5b3hj4x'


'''
INSTANTIATE PLUGINS
'''
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
db = SQLAlchemy(app)
migrate = Migrate(app, db)
mail = Mail(app)

limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["5000 per day", "2000 per hour"],
)

login = LoginManager(app)
login.login_view = 'auth.login'
login.login_message_category = "warning"

''' 
DEFINE CUSTOM DECORATORS 
'''

def role_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
               return login.unauthorized()
            urole = current_user.get_roles()
            if ( ( role not in urole) and (role != "ANY")):
                print("Error!!!")
                return login.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


'''
IMPORT BLUEPRINTS
'''

from backend.site import bp as site_bp
from backend.api import bp as api_bp
from backend.admin import bp as admin_bp
from backend.auth import bp as auth_bp
from backend.errors import bp as errors_bp
from backend.chat import chat as chat_bp

'''
REGISTER BLUEPRINTS
'''

app.register_blueprint(site_bp)
app.register_blueprint(api_bp, url_prefix='/api')
app.register_blueprint(auth_bp, url_prefix='/auth')
app.register_blueprint(admin_bp, url_prefix='/admin')
app.register_blueprint(errors_bp, url_prefix='/error')
app.register_blueprint(chat_bp, url_prefix='/chat')
