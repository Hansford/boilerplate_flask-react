from flask import Blueprint

chat = Blueprint('chat', __name__)

from backend.chat import routes, events