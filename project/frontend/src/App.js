import React, { Component } from 'react';
import styled from 'styled-components';
import logo from './logo.svg';
import './App.css';



class App extends Component {
  render() {

    const Intro = styled.p`
  width: 50%;
  margin: 20px;
  color: ${props => props.color || 'black'}
`
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <p>Test 1: Success </p>
          <Intro> Styled Component Demo </Intro>
          <React.Fragment>
    <Intro>
      Welcome to my  Styled Components demo.
    </Intro>
    <Intro color={'navy'}>
      The next Intro has a bit of colour.
    </Intro>
  </React.Fragment>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
